package main

import (
	"log"
	"os"
	"regexp"
)

func main() {
	wd, _ := os.Getwd()
	cdir, _ := os.Open(wd)
	itemsInDir, _ := cdir.Readdirnames(0)

	srcExt := os.Args[1]
	destExt := os.Args[2]

	var onameB []byte
	var onameS string

	re := regexp.MustCompile(`.*\.` + srcExt + `$`)

	for _, v := range itemsInDir {
		if re.MatchString(v) {
			onameB = []byte(v)
			onameS = string(onameB)
			baseLength := len(v) - len(srcExt)
			basenameB := onameB[0:(baseLength - 1)]
			nnameS := string(basenameB) + "." + destExt
			err := os.Rename(onameS, nnameS)
			if err != nil {
				log.Println(err)
			}
		}
	}
}
