# repex

カレントディレクトリにあるファイルの拡張子の置換します

## 使い方

```sh
$ repex srcExt destExt
```

`jpeg`を`jpg`に置換する

```sh
$ repex jpeg jpg
```

